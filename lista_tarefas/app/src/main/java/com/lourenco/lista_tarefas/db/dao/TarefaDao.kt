package com.lourenco.lista_tarefas.db.dao

import androidx.room.*
import com.lourenco.lista_tarefas.entities.Tarefa

@Dao
interface TarefaDao {

    @Query("select * from tarefas")
    fun getAll():  List<Tarefa>

    @Query("select * from tarefas where id = :id limit 1")
    fun  findById(id: Int): Tarefa?

    @Insert
    fun insert(person:Tarefa):Long

    @Update
    fun update(person:Tarefa)

    @Delete
    fun remove(person:Tarefa)

}