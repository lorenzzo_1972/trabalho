package com.lourenco.lista_tarefas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.lourenco.lista_tarefas.db.AppDatabase
import com.lourenco.lista_tarefas.db.dao.TarefaDao
import com.lourenco.lista_tarefas.entities.Tarefa
import com.lourenco.lista_tarefas.ui.AdapterTarefa
import com.lourenco.lista_tarefas.ui.TarefaAdapterListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TarefaAdapterListener {


    lateinit var dao: TarefaDao
    lateinit var adapter: AdapterTarefa

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db =
            Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java,
                "tarefa.db"
            )
                .allowMainThreadQueries()
                .build()
        dao = db.tarefaDao()

        btAdicionar.setOnClickListener{
            addicionarCard()
        }
        carregarTarefas()

    }


    private fun addicionarCard() {
        val pos = adapter.addCard( Tarefa("","",""))
        listaTarefas.scrollToPosition(pos)
    }

    private fun carregarTarefas() {
        val tarefas = dao.getAll()
        adapter = AdapterTarefa(tarefas.toMutableList(),this)
        listaTarefas.adapter = adapter
        listaTarefas.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )
    }

    override fun saveTarefa(tarefa: Tarefa, position: Int) {
        val id = dao.insert(tarefa)
        val task = dao.findById(id.toInt())
        adapter.saveTarefa(task!!, position)
        listaTarefas.scrollToPosition(position)    }

    override fun deleteTarefa(tarefa: Tarefa) {
        dao.remove(tarefa)
    }

    override fun updateTarefa(tarefa: Tarefa) {
        dao.update(tarefa)
    }
}
