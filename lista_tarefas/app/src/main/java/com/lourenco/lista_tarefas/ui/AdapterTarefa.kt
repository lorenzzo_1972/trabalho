package com.lourenco.lista_tarefas.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lourenco.lista_tarefas.R
import com.lourenco.lista_tarefas.entities.Tarefa
import kotlinx.android.synthetic.main.card_editing.view.*
import kotlinx.android.synthetic.main.card_view.view.*


class AdapterTarefa(private var tarefas:MutableList<Tarefa>, private var listener:TarefaAdapterListener):
    RecyclerView.Adapter<AdapterTarefa.ViewHolder>(){

    private var tarefasEditando:Tarefa? = null


    override fun getItemViewType(position: Int): Int {
        val tarefa:Tarefa= tarefas[position]
        if (tarefasEditando == tarefa)
            return R.layout.card_editing
        else
            return R.layout.card_view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
    )


    override fun getItemCount()= tarefas.size

    fun saveTarefa(tarefa: Tarefa, position: Int){
        tarefas.set(position, tarefa)
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tarefa = tarefas[position]
        holder.preencherView(tarefa)
    }

    fun remove(posicao: Int){

        tarefas.removeAt(posicao)
        notifyItemRemoved(posicao)
    }

    fun update(tarefa: Tarefa, posicao: Int){
        tarefas.set(posicao, tarefa)
        tarefasEditando = null
        notifyItemChanged(posicao)
    }

    fun addCard(tarefa: Tarefa): Int {
        tarefas.add(0,tarefa)
        tarefasEditando = tarefa
        notifyItemInserted(0)
        return 0
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun preencherView(tarefa: Tarefa){
            if (tarefasEditando == tarefa){
                itemView.txtTitleEditing.setText(tarefa.title)
                itemView.txtDescEditing.setText(tarefa.description)
                itemView.btSalvar.setOnClickListener{
                    with(this@AdapterTarefa) {

                        val title = itemView.txtTitleEditing.text.toString()
                        val description = itemView.txtDescEditing.text.toString()
                        var status = tarefa.status
                        if (tarefa.status == ""){
                            status =  "NÃO FEITA"
                        }
                        val position = tarefas.indexOf(tarefa)


                            if (tarefa.id == null) {
                                listener.saveTarefa(Tarefa(title, description, status),position)
                            }else {
                                var tarefaAux = Tarefa(title,description,status)
                                tarefaAux.id = tarefa.id
                                update(tarefaAux,position)
                                listener.updateTarefa(tarefaAux)
                            }

                    }
                }
            }

            else{
                itemView.txtTitle.setText(tarefa.title)
                itemView.txtStatus.setText(tarefa.status)

                itemView.setOnLongClickListener {

                    var novatarefa = tarefa
                    if (tarefa.status.equals("NÃO FEITA")){

                        novatarefa.status="FEITA"

                    }
                    else
                    {
                        novatarefa.status="NÃO FEITA"

                    }
                    var position =  tarefas.indexOf(tarefa)
                    update(novatarefa,position)
                    listener.updateTarefa(novatarefa)
                    false



                }
                itemView.setOnClickListener {
                    val posicao = tarefas.indexOf(tarefa)
                    tarefasEditando = tarefas[posicao]
                    notifyItemChanged(posicao)
                }

                itemView.btRemover.setOnClickListener{
                    val posicao = tarefas.indexOf(tarefa)
                    remove(posicao)
                    listener.deleteTarefa(tarefa)
                }


                itemView.btCompartilhar.setOnClickListener{
                    if (tarefa.status.equals("FEITA"))
                    {
                        val mensagem= "Ótimo, tarefa concluída!!!"
                        val intent:Intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, "$mensagem:${tarefa.title}")
                            type = "text/plain"
                        }
                        itemView.context.startActivity(intent)
                    }
                }
            }
        }
    }
}