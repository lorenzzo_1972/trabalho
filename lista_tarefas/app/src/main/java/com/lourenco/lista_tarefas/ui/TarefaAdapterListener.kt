package com.lourenco.lista_tarefas.ui

import com.lourenco.lista_tarefas.entities.Tarefa

interface TarefaAdapterListener {
    fun saveTarefa(tarefa: Tarefa, position: Int)
    fun deleteTarefa(tarefa: Tarefa)
    fun updateTarefa(tarefa: Tarefa)
}