package com.lourenco.lista_tarefas.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
data class Tarefa(

    var title:String,
    var description:String,
    var status:String = ""
) {

    @PrimaryKey(autoGenerate = true)
    var id :Int?=null
}