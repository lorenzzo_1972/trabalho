package com.lourenco.lista_tarefas.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lourenco.lista_tarefas.db.dao.TarefaDao
import com.lourenco.lista_tarefas.entities.Tarefa


// criação do banco de dados sqlite

@Database(version = 1, entities = arrayOf(Tarefa::class), exportSchema = true)
abstract class AppDatabase:RoomDatabase() {
    abstract fun tarefaDao():TarefaDao
}